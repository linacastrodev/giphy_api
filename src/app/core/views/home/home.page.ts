import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { GifService } from 'src/app/customers/services/gif.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  dataGift: any;
  constructor(private dataService: GifService, private toastController: ToastController) { }

  ngOnInit() {
    this.showAllGifs();
    this.welcomeMessage();
  }

  async showAllGifs() {
    try {
      await this.dataService.getByIDs().subscribe(data => {
        this.dataGift = data.data;
        return this.dataGift;
      });
    } catch (error) {
      console.log(error);
    }
  }

  async welcomeMessage() {
    const toast = await this.toastController.create({
      message: 'Welcome to Giphy Icons Api, is a pleasure!',
      duration: 5000,
      position: "bottom"
    });
    toast.present();
  }
}
