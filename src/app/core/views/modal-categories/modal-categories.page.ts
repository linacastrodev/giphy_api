import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { GifService } from 'src/app/customers/services/gif.service';

@Component({
  selector: 'app-modal-categories',
  templateUrl: './modal-categories.page.html',
  styleUrls: ['./modal-categories.page.scss'],
})
export class ModalCategoriesPage implements OnInit {
  data:any;
  constructor(private dataService: GifService, private modalController:ModalController) { }

  ngOnInit() {
    this.showDataCategories();
  }
  async showDataCategories() {
    try {
      await this.dataService.getByCategories().subscribe(data => {
        this.data = data.data;
        return this.data;
      })
    } catch (error) {
      console.error(error)
    }
  }
  closeModal(){
    this.modalController.dismiss();
  }
}
