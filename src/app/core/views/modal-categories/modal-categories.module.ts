import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalCategoriesPageRoutingModule } from './modal-categories-routing.module';

import { ModalCategoriesPage } from './modal-categories.page';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    IonicModule,
    ModalCategoriesPageRoutingModule
  ],
  declarations: [ModalCategoriesPage]
})
export class ModalCategoriesPageModule {}
