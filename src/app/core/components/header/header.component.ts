import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalCategoriesPage } from '../../views/modal-categories/modal-categories.page';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  constructor(private modalController:ModalController) { }

  ngOnInit() {}

  async goToModal(){
    const modal = await this.modalController.create({
      component: ModalCategoriesPage,
    });
    return await modal.present();
  }
}
