import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class GifService {
  public apiUrl = {
   id: 'https://api.giphy.com/v1/gifs/trending?api_key=s7m4RqRGGPTsLwSU2q6m6yncDRbrCSyJ&limit=12&rating=g',
   categories: 'https://api.giphy.com/v1/gifs/categories?api_key=s7m4RqRGGPTsLwSU2q6m6yncDRbrCSyJ'
  };
  constructor(private http: HttpClient) {
  }
  getByIDs(): Observable<any> {
    try {
      return this.http.get(this.apiUrl.id);
    } catch (error) {
      console.error(error);
    }
  }
  getByCategories(): Observable<any>{
    try {
      return this.http.get(this.apiUrl.categories)
    } catch (error) {
        console.error(error)
    }
  }
}
